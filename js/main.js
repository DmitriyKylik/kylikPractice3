'use strict';
var colorBlocks = document.querySelectorAll('.colors>.color');
var gameBoard = document.querySelector('.gameBoard');
var gameData = document.querySelectorAll('.gameData');
var resetBtn = document.querySelector('.resetBtn');
var sequence = document.querySelector('.sequence');
var moveCounter = document.querySelector('.moves');
var aiBtn = document.querySelector('.aiBtn');
var showedColors = [];
var indexedGameBoard = [];
var moves = 0;
var rowBoard;
var colBoard;
var colorsAmount;
var count = 0;
var countingMode = false;
var prevColor;
var aiMap = {};
var oldColor;
var search;

function getValues(e) {
    e.preventDefault();
    colorsAmount = +(e.target.elements['colors'].value);
    rowBoard = +(e.target.elements['rows'].value);
    colBoard = +(e.target.elements['columns'].value);

    if(inputCheck (colorsAmount, rowBoard, colBoard )){
        hideShowData(gameData);
        showColors(colorsAmount, colorBlocks);
        clear(gameBoard);
        toggleDisableElem(resetBtn);
        createGameBoard(gameBoard, showedColors, rowBoard, colBoard);
    }
}

function showColors(colorsAmount, colorsToShow) {
    for(var i = 0; i < colorsAmount; i++) {
        colorsToShow[i].classList.toggle('hide');
        showedColors[i] = colorsToShow[i].dataset.color;
    }
}

function fillColor(boardBlock, newColor, countingMode) {
    changeColor(boardBlock, 0, 0, newColor);
    checkForFlood(boardBlock, newColor, countingMode);
    oldColor = newColor;
    appendInSequence(newColor, sequence);
    moves++;
    showCountMoves(moves);
    if( finished(boardBlock, newColor) ) {
        setTimeout(function() {
            alert("Well Done!");
            resetGame(gameBoard, showedColors, rowBoard, colBoard);
        }, 600);
    }
}

function testNeighbors(boardBlock, row, col, color) {

    if( row < rowBoard -1 ) {
        checkColorNeighbors(boardBlock, row + 1, col, color);
    }
    if( row > 0 ) {
        checkColorNeighbors(boardBlock, row - 1, col, color);
    }
    if( col < colBoard - 1 ) {
        checkColorNeighbors(boardBlock, row, col + 1, color);
    }
    if( col > 0 ) {
        checkColorNeighbors(boardBlock, row, col - 1, color);
    }
}

function checkColorNeighbors(boardBlock, row, col, color) {

    if(boardBlock[row][col].colored) {
        return;
    }

    if( boardBlock[row][col].color === color ) {
        count++;
        boardBlock[row][col].colored = true;
        testNeighbors(boardBlock, row, col, color);
    }
}

function createGameBoard(boardBlock, colorData, rows, col) {

    for(var i = 0; i < rows; i++) {
        var row = createElem('tr', boardBlock);
        indexedGameBoard[i] = [];

        for(var j = 0; j < col; j++) {
            var tile = createElem('td', row);
            var random = Math.floor(Math.random() * colorData.length - 1) + 1;
            var backGroundColor = colorData[random];

            tile.classList.add('gameTile', backGroundColor);
            indexedGameBoard[i][j] = {
                color: backGroundColor,
                colored: false
            };
        }
    }
    indexedGameBoard[0][0].colored = true;
    fillColor(indexedGameBoard, indexedGameBoard[0][0].color, countingMode);
}

function changeColor(boardBlock, row, col, newColor) {
    var tile = gameBoard.children[row].children[col];

    tile.classList.remove(boardBlock[row][col].color);
    boardBlock[row][col].color = newColor;
    tile.classList.add(newColor);
}

function createElem(type, parent) {
    var elem = document.createElement(type);
    parent.appendChild(elem);
    return elem;
}

function resetGame(gameBoard, colorData, rows, col) {
    var restart = confirm("New Game?");
    moves = 0;
    clear(gameBoard);
    clear(sequence);
    clear(moveCounter);
    clearInterval(search);
    toggleDisableElem(aiBtn);
    if (restart) {
        hideShowData(gameData);
        toggleDisableElem(resetBtn);
        toggleDisableElem(resetBtn);
        showColors(colorsAmount, colorBlocks);
        showedColors = [];
    } else {
        createGameBoard(gameBoard, showedColors, rows, col);
    }
}

function clear(gameBoard) {
    gameBoard.innerHTML = '';
}

function hideShowData(gameData) {
    for(var i = 0; i < gameData.length; i++) {
        gameData[i].classList.toggle('hide');
    }
}

function toggleDisableElem(elem) {
    elem.disabled !== elem.disabled;
    if(elem.disabled){
        elem.disabled = false;
    }else{
        elem.disabled = true;
    }
}

function inputCheck(colorsAmount, rowBoard, colBoard ) {
    if(rowBoard * colBoard < colorsAmount) {
        alert("Your tile amount is: " + rowBoard * colBoard + " It  mustn't be less than availbale colors amount: (" + colorsAmount + ")");
        return false;
    }
    return true;
}

function appendInSequence(color, parent) {
    var colorInSequence =  createElem('p', parent);
    colorInSequence.classList.add('sequenceColor');
    colorInSequence.textContent = color;
}

function showCountMoves(moves) {
    moveCounter.textContent = moves;
}

function finished(indexedGameBoard, newColor) {
    for(var i = 0; i < indexedGameBoard.length; i++) {
        for(var j = 0; j < indexedGameBoard.length; j++) {
            if( indexedGameBoard[i][j].color === newColor ) {
                continue;
            }
            return false;
        }
    }
    return true;
}

function countAvailableColors(availColors, indexedGameField, countingMode) {
    countingMode = true;
    var color;
    var counter = 0;
    for(var i = 0; i < availColors.length; i++) {
        if(availColors[i] !== oldColor) {
            checkForFlood(indexedGameField, availColors[i], countingMode);
            if(counter <= aiMap.count) {
                counter = aiMap.count;
                color = aiMap.color;
            }
            clearFlooded(indexedGameField, aiMap.color);
        }
    }
    return color;
}

function checkForFlood(boardBlock, targetColor, countingMode) {
    count = 0;
    boardBlock[0][0].colored = true;
    for(var i = 0; i < rowBoard; i++) {
        for(var j = 0; j < colBoard; j++) {
            if(boardBlock[i][j].colored) {
                testNeighbors(boardBlock, i, j, targetColor, countingMode);
                if(boardBlock[i][j].color !== targetColor && !countingMode) {
                    changeColor(boardBlock, i, j, targetColor);
                }
            }
        }
    }
    aiMap.color = targetColor;
    aiMap.count = count;
}

function runAi (indexedgameBoard, colorList, countingMode) {
    toggleDisableElem(aiBtn);
    search = setInterval( function() {
        var newColor = countAvailableColors (colorList, indexedgameBoard, countingMode);
        countingMode = false;
        fillColor(indexedGameBoard, newColor, countingMode);
    }, 1500);
}

function clearFlooded(gameBoardArray, targetColor) {
    for(var i = 0; i < gameBoardArray.length; i++) {
        for(var j = 0; j < gameBoardArray[i].length; j++) {
            if(gameBoardArray[i][j].colored && gameBoardArray[i][j].color === targetColor) {
                gameBoardArray[i][j].colored = false;
            }
        }
    }
}

    // on Click on ai button start the function that goes through array.
    // Apparantely, it returns us the color, what we should fill as parameter to fillColorFunction.
    // As a parameter to itself it gets the color of the first element
    // It goes through array, and search for the old colors
    //
    // And here it comes the problem!
    // if aiMode is on, function must get the first element of the showedColors array. and start the move through array


    // 1) Take and add third property to indexedArray elem - counted;
    // 2) Make checkColorNeighbors more abstractive, pass as the parameter, on parameter it should check 'colored' or 'counted' which
    //

    // disable color blocks
    // disable reset button
    // take the colors that we have showedColors
    // We need to take a fillColor function
    // We need to take a part of it actually
    // If there is a tie take available color take Math.min.apply(Math, )
    // get the min index from showedColors array

    // A part that floods the oldColor
    // Or we need to add the par
    // if countingMode === true then instead of setting tile colored = true, we make count++;
    // count === 1;



// function checkColorNeighbors(boardBlock, row, col, color) {

//     if(boardBlock[row][col].colored) {
//         return;
//     }

//     if( boardBlock[row][col].color === color ) {
//         if(countingMode === true) {
//             count++;
//             testNeighbors(boardBlock, row, col, color);
//         }else {
//             boardBlock[row][col].colored = true;
//             testNeighbors(boardBlock, row, col, color);
//         }
//     }
// }

